FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://settings \
            file://wifi.config \
            file://main.conf\
"

do_install_append() {
	install -d ${D}${localstatedir}/lib/connman/
	install -d ${D}${sysconfdir}/connman/	
	install -m 0644 ${WORKDIR}/settings ${D}${localstatedir}/lib/connman/
	install -m 0644 ${WORKDIR}/wifi.config ${D}${localstatedir}/lib/connman/
	install -m 0644 ${WORKDIR}/main.conf  ${D}${sysconfdir}/connman/
}


PACKAGES =+ "${PN}-settings"

FILES_${PN}-settings = "${localstatedir}/lib/connman/settings"
FILES_${PN}-settings += "${localstatedir}/lib/connman/wifi.config"
FILES_${PN}-settings += "${sysconfdir}/connman/main.conf"

RDEPENDS_${PN}-settings = "${PN}"
LICENSE_FLAGS_${PN}-settings = "non-commercial"

#PACKAGECONFIG_append_pn-connman = " systemd wifi 3g bluez"
PACKAGECONFIG_append_pn-connman = " systemd wifi 3g"
