DESCRIPTION = "libtorrent is an open source C++ library implementing the BitTorrent protocol"
HOMEPAGE = "http://libtorrent.org"
SECTION = "libs/network"
LICENSE = "BSD-2-Clause"
DEPENDS = "openssl boost"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"

inherit autotools

EXTRA_OECONF="--with-boost-libdir=${STAGING_LIBDIR}"
