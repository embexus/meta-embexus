require libtorrent-rasterbar.inc

SRC_URI[md5sum] = "3a291044b5b33fec3e30b22a94fda31f"
SRC_URI[sha256sum] = "2713df7da4aec5263ac11b6626ea966f368a5a8081103fd8f2f2ed97b5cd731d"

SRC_URI = "https://github.com/arvidn/libtorrent/releases/download/libtorrent-1_1/${PN}-${PV}.tar.gz"
