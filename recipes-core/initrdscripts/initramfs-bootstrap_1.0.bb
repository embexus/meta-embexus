SUMMARY = "basic initramfs image init script"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
SRC_URI = "file://init-boot.sh"


S = "${WORKDIR}"

do_install() {
        install -d ${D}${base_sbindir}
        install -m 0755 ${WORKDIR}/init-boot.sh ${D}${base_sbindir}/init
}

do_install_append() {
        install -d ${D}/dev
        mknod -m 622 ${D}/dev/console c 5 1
        mknod -m 666 ${D}/dev/null c 1 3
}

inherit allarch

FILES_${PN} += "/dev /sbin/init "
