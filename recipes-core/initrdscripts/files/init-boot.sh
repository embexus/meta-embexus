#!/bin/sh

PATH=/sbin:/bin:/usr/sbin:/usr/bin

ROOTDEV="/dev/mmcblk1p1"

mkdir /proc /sys /tmp

mount -t devtmpfs none /dev
mount -t proc proc /proc
mount -t sysfs sysfs /sys
mount -t tmpfs tmpfs /tmp

#wait a bit!
usleep 100000

# Mount the root filesystem.
mkdir /root
mount -o,ro ${ROOTDEV} /root

# Clean up.
umount /proc
umount /sys

# Boot the real thing.
exec switch_root /root /sbin/preinit
