DESCRIPTION = "qt5 demo image"

require embexus-image-base.bb

PR="r5"

IMAGE_INSTALL += " \
    tslib \
    tslib-calibrate \
    fontconfig \
    cantarell-fonts \
"
IMAGE_INSTALL += " \
    qt-in-automotive \
"
export IMAGE_BASENAME = "embexus-image-qt5demo"
