SUMMARY = "Emebxus Demo image for network appliance"
HOMEPAGE = "http://embexus.com"

require recipes-core/images/embexus-image-base.bb

LICENSE = "MIT"

RTLWIFI_FW = "linux-firmware-rtl8192cu linux-firmware-rtl8192ce linux-firmware-rtl8192su"

IMAGE_INSTALL_append = "connman-client \
                       ${RTLWIFI_FW}"


export IMAGE_BASENAME = "embexus-image-hotspot"

