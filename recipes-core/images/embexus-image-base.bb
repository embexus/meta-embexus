SUMMARY = "Base image for Embexus Distro"
HOMEPAGE = "https://embexus.com"

IMAGE_FEATURES += "package-management ssh-server-dropbear"
IMAGE_LINGUAS = "en-us"

LICENSE = "MIT"

inherit core-image
inherit image_types

CONMANPKGS = "connman connman-settings"

IMAGE_INSTALL_append = "\
	${CONMANPKGS} \
	${CORE_IMAGE_BASE_INSTALL} \
"
