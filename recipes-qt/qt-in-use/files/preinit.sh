#!/bin/busybox sh


echo "-> PreInit Started..."

## Put your time-critical application start here !
#dd if=/usr/share/screenshot.fb of=/dev/fb0 bs=1M > /dev/null 2>&1 

echo "-> PreInit done !"

# start real init (systemd/SysVinit)
exec /sbin/init
