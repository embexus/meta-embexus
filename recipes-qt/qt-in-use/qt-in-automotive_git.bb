DESCRIPTION = "Demo from automotive IVI"

LICENSE = "LGPLv2.1+ & GFDL-1.2"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=f7f3422e093a2b8e49cb5efac1b7ab47"
DEPENDS += "qtbase qtwebkit qtquick1"
RDEPENDS_${PN} += "qtdeclarative qtdeclarative-qmlplugins qtlocation qtquickcontrols qtsvg qtquick1 qtwebkit"

inherit qmake5

BRANCH = "master"
SRC_URI="git://bitbucket.org/embexus/qt-in-automotive.git;protocol=http;branch=${BRANCH}"

SRCREV = "0b439f38b7991ee66497f1bd667d369dbbb96f2a"   

S = "${WORKDIR}/git/qt-quick-ivi-demo"

FILES_${PN} += "${datadir}/ConnectedCarIVI ${datadir}/icons"

EXTRA_OEMAKE += "INSTALL_ROOT=${D}"

do_configure_prepend() {
	# fix qmake ugly deployment support
	find ${S} -type f -exec sed -i 's,/usr/local,/usr,g' '{}' ';'
}


inherit systemd

SRC_URI_append = " file://carivi.service \
                   file://preinit.sh"


SYSTEMD_SERVICE_${PN} = "carivi.service"
SYSTEMD_PACKAGES = "${PN}"

do_install_append() {

	install -d ${D}/lib/systemd/system ${D}/${base_sbindir}
	install -m 0644 ${WORKDIR}/carivi.service ${D}/lib/systemd/system
        install -m 0755 ${WORKDIR}/preinit.sh     ${D}/${base_sbindir}/preinit
}
