FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append         = " file://printk.cfg \
                           file://standard.cfg \
                           file://rtl8192cu.cfg \
                           file://r8712u.cfg \
                           file://tethering.cfg \
                           file://serial-all.cfg \
                           file://overlayfs.cfg \
                           file://bluetooth.cfg \
                           file://bluetooth-usb.cfg\
                           file://power.cfg"

KERNEL_MODULE_AUTOLOAD_append = " ip_tables tun"
