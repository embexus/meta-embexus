DESCRIPTION = "GPS Track application"
HOMEPAGE = "https://embexus.com/"
SECTION = "libs/network"
LICENSE = "GPLv2"
DEPENDS = "libpubnub curl json-c openssl libevent"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"

BRANCH="master"
SRCREV   = "${AUTOREV}"
SRC_URI  = "git://github.com/embexus/gpstrack.git;protocol=http;branch=${BRANCH}"
SRC_URI += "file://pubnub.credential.patch"
SRC_URI += "file://gpstrack.service"

INSANE_SKIP_${PN} = "ldflags"

inherit systemd

SYSTEMD_SERVICE_${PN} = "gpstrack.service"
SYSTEMD_PACKAGES += "${PN}"

S="${WORKDIR}/git"

do_compile() {
	unset LDFLAGS
	oe_runmake
}

do_install() {
	install -d ${D}${bindir} ${D}${bindir}
	install -m 0755 ${S}/gpstrack ${D}${bindir}/gpstrack
}

do_install_append() {
	install -d ${D}/lib/systemd/system
	install -m 0644 ${WORKDIR}/gpstrack.service ${D}/lib/systemd/system
}

FILES_${PN} += "${bindir}/gpstrack"
