DESCRIPTION = "PubNub client for C"
HOMEPAGE = "http://www.pubnub.com/"
SECTION = "libs/network"
LICENSE = "MIT"
DEPENDS = "json-c openssl libevent curl"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690"

SRC_URI = "https://github.com/pubnub/c/archive/${PN}-${PV}.tar.gz"
SRC_URI += "file://libpubnub.build.patch"

inherit pkgconfig

INSANE_SKIP_${PN} = "ldflags" 

S="${WORKDIR}/c-libpubnub-${PV}"

do_install() {

    install -d ${D}${libdir} ${D}${includedir}
    install -m 0644 ${S}/libpubnub/pubnub*.h ${D}${includedir}/
    install -m 0755 ${S}/libpubnub/libpubnub.so.${PV} ${D}${libdir}/libpubnub.so.${PV}
    ln -s libpubnub.so.${PV} ${D}${libdir}/libpubnub.so

}

FILES_${PN} += "${libdir}/lib*.so*"
